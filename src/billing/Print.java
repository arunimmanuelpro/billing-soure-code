/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package billing;

import dao.Database;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.PrintJob;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaSizeName;
import javax.swing.JComponent;
import javax.swing.JTable;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Arun
 */
public class Print extends javax.swing.JFrame {

    public int sno = 1;
    DefaultTableModel defaultTableModel;
    DefaultTableModel taxTableModel;
    Set<Float> percentage;
    
    /**
     * Creates new form Print
     */
    public Print() {
        initComponents();
        txtDate.setText(Utility.getDate());
        hideLabels();
    }
    public Print(int invoiceNumber){
        percentage = new HashSet<>();
        initComponents();
        txtDate.setText(Utility.getDate());
        hideLabels();
        if(invoiceNumber != 0){
            float total = 0.0f;
            float unitTotal = 0.0f;
            txtInvoiceNumber.setText(String.valueOf(invoiceNumber));
            defaultTableModel = (DefaultTableModel)tblBill.getModel();
            taxTableModel= (DefaultTableModel) tblTax.getModel();
            try{
                Connection connection = Database.getConnections();
                PreparedStatement preparedStatement = connection.prepareStatement("select * from invoice where invoiceId = ?");
                preparedStatement.setInt(1, invoiceNumber);
                ResultSet resultSet = preparedStatement.executeQuery();
                while(resultSet.next()){
                    percentage.add(resultSet.getFloat("igstrate"));
                    preparedStatement = connection.prepareStatement("select * from items where code = ?");
                    preparedStatement.setString(1, resultSet.getString("productCode"));
                    ResultSet resultSet2 = preparedStatement.executeQuery();
                    if(resultSet2.next()){
                        float discount = resultSet.getFloat("discount");
                        float quantity = resultSet.getFloat("qty");
                        float price = resultSet.getFloat("price");
                        float discountPrice = (price * quantity) - (((quantity * price)/100) * discount);          
                        float igst = (discountPrice/100)*resultSet.getFloat("igstrate");
                        float amount = discountPrice + igst;
                        defaultTableModel.addRow(new Object[]{sno,resultSet2.getString("description"),resultSet.getString("productCode"),resultSet2.getInt("hsn"),resultSet.getFloat("qty"),resultSet2.getString("unit"),resultSet.getFloat("price"),resultSet.getFloat("igstrate")+"%",igst,amount});
                        sno++;
                        total +=resultSet.getFloat("amount");
                        unitTotal += resultSet.getFloat("qty");
                    } 
                    tblBill.setPreferredScrollableViewportSize(tblBill.getPreferredSize());
                }
                lblActual.setText(String.valueOf(total));
                lblGrandTotal.setText("Grand Total    "+unitTotal+" Units");
                lblRoundOff.setText(String.valueOf(Math.round(total) - total));
                if((Math.round(total) - total)>0){
                    lblDesc.setText("Add : Rounded off(+)");
                }else{
                    lblDesc.setText("Sub : Rounded off(-)");
                }
                txtTotal.setText(String.valueOf(Math.round(total)));
                lblRupee.setText(Utility.convertToWord(Math.round(total))+" Only");
                preparedStatement = connection.prepareStatement("select distinct invoice_master.id,user.id, user.name,user.address,user.state,user.gstin from user join invoice_master on user.id=invoice_master.userId where invoice_master.id=?");
                preparedStatement.setInt(1, invoiceNumber);
                resultSet = preparedStatement.executeQuery();
                if(resultSet.next()){
                    lblBilledAddress.setText(resultSet.getString("address"));
                    lblShippedAddress.setText(resultSet.getString("address"));
                    lblState.setVisible(true);
                    lblState2.setVisible(true);
                    lblGSTIN.setVisible(true);
                    lblGstin2.setVisible(true);
                    lblState.setText(resultSet.getString("state"));
                    lblState2.setText(resultSet.getString("state"));
                    lblGSTIN.setText(resultSet.getString("gstin"));
                    lblGstin2.setText(resultSet.getString("gstin"));
                }
                float totalTaxAmount = 0.0f;
                float totalIgst = 0.0f;
                for(float i : percentage){
                    float taxableAmount=0.0f;
                    float igst = 0.0f;
                    preparedStatement = connection.prepareStatement("select * from invoice where igstrate = ? and invoiceid = ?");
                    preparedStatement.setFloat(1, i);
                    preparedStatement.setInt(2, invoiceNumber);
                    resultSet = preparedStatement.executeQuery();
                    while(resultSet.next()){
                        taxableAmount += resultSet.getFloat("qty")*resultSet.getFloat("price");
                        igst +=resultSet.getFloat("igstamount");
                    }
                    totalTaxAmount +=taxableAmount;
                    totalIgst += igst;
                    taxTableModel.addRow(new Object[]{i+"%",taxableAmount,igst,igst});
                }
                taxTableModel.addRow(new Object[]{"Totals",totalTaxAmount,totalIgst,totalIgst});
                tblTax.setPreferredScrollableViewportSize(tblTax.getPreferredSize());
                connection.close();
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnPrint = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        panelPrint = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtGstin = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtInvoiceNumber = new javax.swing.JLabel();
        txtDate = new javax.swing.JLabel();
        txtPlaceOfSupply = new javax.swing.JTextField();
        txtReverse = new javax.swing.JTextField();
        txtGrNo = new javax.swing.JTextField();
        lblPlace = new javax.swing.JLabel();
        lblReverse = new javax.swing.JLabel();
        lblGrNo = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txtTransport = new javax.swing.JTextField();
        txtVehicleNo = new javax.swing.JTextField();
        txtStation = new javax.swing.JTextField();
        txtPoNo = new javax.swing.JTextField();
        txtNoofPacks = new javax.swing.JTextField();
        lblTransport = new javax.swing.JLabel();
        lblVehicleNo = new javax.swing.JLabel();
        lblStation = new javax.swing.JLabel();
        lblPo = new javax.swing.JLabel();
        lblNoofPacks = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        lblBilledAddress = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        lblShippedAddress = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        lblState = new javax.swing.JLabel();
        lblGSTIN = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        lblState2 = new javax.swing.JLabel();
        lblGstin2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblBill = new javax.swing.JTable();
        txtTotal = new javax.swing.JLabel();
        lblActual = new javax.swing.JLabel();
        lblDesc = new javax.swing.JLabel();
        lblRoundOff = new javax.swing.JLabel();
        lblGrandTotal = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblTax = new javax.swing.JTable();
        lblRupee = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        txtBankDetails = new javax.swing.JTextField();
        lblBankDetails = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Print Preview");

        btnPrint.setText("Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        panelPrint.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel2.setText("GSTIN : ");

        txtGstin.setText("123456789");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Original for Buyer");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("TAX INVOICE");

        jLabel6.setText("Invoice No.             :");

        jLabel7.setText("Date of Invoice      :");

        jLabel8.setText("Place of Supply     :");

        jLabel9.setText("Reverse Charge   :");

        jLabel10.setText("GR/RR No.             :");

        txtInvoiceNumber.setText("44");

        txtDate.setText("jLabel11");

        txtPlaceOfSupply.setText("Tamil Nadu (33)");
        txtPlaceOfSupply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPlaceOfSupplyActionPerformed(evt);
            }
        });
        txtPlaceOfSupply.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPlaceOfSupplyKeyPressed(evt);
            }
        });

        txtReverse.setText("N");
        txtReverse.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtReverseKeyPressed(evt);
            }
        });

        txtGrNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtGrNoActionPerformed(evt);
            }
        });
        txtGrNo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtGrNoKeyPressed(evt);
            }
        });

        lblPlace.setText("jLabel11");

        lblReverse.setText("jLabel12");

        lblGrNo.setText("jLabel13");

        jLabel11.setText("Transport       :");

        jLabel12.setText("Vehicle No.     :");

        jLabel13.setText("Station              :");

        jLabel14.setText("PO Number     :");

        jLabel15.setText("No. of Packs    :");

        txtTransport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTransportActionPerformed(evt);
            }
        });

        txtVehicleNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtVehicleNoActionPerformed(evt);
            }
        });

        lblTransport.setText("jLabel16");

        lblVehicleNo.setText("jLabel16");

        lblStation.setText("jLabel16");

        lblPo.setText("jLabel16");

        lblNoofPacks.setText("jLabel16");

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel16.setText("Billed to  :");

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel17.setText("Shipped to :");

        jLabel18.setText("State");

        jLabel19.setText("GSTIN / UIN");

        lblState.setText("jLabel20");

        lblGSTIN.setText("jLabel20");

        jLabel20.setText("State");

        jLabel21.setText("GSTIN / UIN");

        lblState2.setText("jLabel22");

        lblGstin2.setText("jLabel22");

        tblBill.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "S.No", "Description", "Product Code", "HSN/SAC Code", "Qty.", "Unit", "Price", "IGST Rate", "IGST Amount", "Amount(Rs.)"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class, java.lang.Float.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblBill.setFillsViewportHeight(true);
        tblBill.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                tblBillComponentResized(evt);
            }
        });
        jScrollPane1.setViewportView(tblBill);

        txtTotal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        txtTotal.setText("Total");

        lblActual.setText("Actual");

        lblDesc.setText("Description");

        lblRoundOff.setText("Round Off");

        lblGrandTotal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblGrandTotal.setText("Unit Total");

        tblTax.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Tax Rate", "Taxable Amount", "IGST", "Total Tax"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Float.class, java.lang.Float.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblTax.setFillsViewportHeight(true);
        jScrollPane2.setViewportView(tblTax);

        lblRupee.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblRupee.setText("Amount in Words");

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel22.setText("Bank Details   : ");

        lblBankDetails.setText("jLabel23");

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel23.setText("Terms & Conditions");

        jLabel24.setText("E & O.E.");

        jLabel25.setText("1. Goods once sold will not be taken back.");

        jLabel26.setText("2. Interest @ 18% p.a. will be charged if the payment");

        jLabel27.setText("is not made with in the stipulated time.");

        jLabel28.setText("3. Subject to \"Uttar Pradesh\" Jurisdiction only.");

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel29.setText("Receiver's Signature   : ");

        jLabel30.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel30.setText("for MAYA HOMES");

        jLabel31.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel31.setText("Authorised Signature");

        javax.swing.GroupLayout panelPrintLayout = new javax.swing.GroupLayout(panelPrint);
        panelPrint.setLayout(panelPrintLayout);
        panelPrintLayout.setHorizontalGroup(
            panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPrintLayout.createSequentialGroup()
                .addComponent(jLabel23)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel29)
                .addGap(154, 154, 154))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPrintLayout.createSequentialGroup()
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelPrintLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(lblActual))
                    .addComponent(jScrollPane1))
                .addGap(10, 10, 10))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPrintLayout.createSequentialGroup()
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelPrintLayout.createSequentialGroup()
                        .addComponent(jLabel24)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel30))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelPrintLayout.createSequentialGroup()
                        .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelPrintLayout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(18, 18, 18)
                                .addComponent(txtInvoiceNumber))
                            .addGroup(panelPrintLayout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(18, 18, 18)
                                .addComponent(txtDate))
                            .addComponent(jLabel16)
                            .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelPrintLayout.createSequentialGroup()
                                    .addComponent(jLabel19)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblGSTIN))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelPrintLayout.createSequentialGroup()
                                    .addComponent(jLabel18)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblState))
                                .addComponent(lblBilledAddress, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelPrintLayout.createSequentialGroup()
                                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelPrintLayout.createSequentialGroup()
                                        .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel9)
                                            .addComponent(jLabel10))
                                        .addGap(18, 18, 18)
                                        .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(txtGrNo)
                                            .addComponent(txtReverse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(panelPrintLayout.createSequentialGroup()
                                        .addComponent(jLabel8)
                                        .addGap(18, 18, 18)
                                        .addComponent(txtPlaceOfSupply, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblReverse)
                                    .addComponent(lblGrNo)
                                    .addComponent(lblPlace))))
                        .addGap(83, 83, 83)
                        .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelPrintLayout.createSequentialGroup()
                                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelPrintLayout.createSequentialGroup()
                                        .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel13)
                                            .addComponent(jLabel14)
                                            .addComponent(jLabel15))
                                        .addGap(18, 18, 18)
                                        .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtPoNo, javax.swing.GroupLayout.DEFAULT_SIZE, 137, Short.MAX_VALUE)
                                            .addComponent(txtNoofPacks)
                                            .addComponent(txtStation)))
                                    .addGroup(panelPrintLayout.createSequentialGroup()
                                        .addComponent(jLabel12)
                                        .addGap(21, 21, 21)
                                        .addComponent(txtVehicleNo))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPrintLayout.createSequentialGroup()
                                        .addComponent(jLabel11)
                                        .addGap(18, 18, 18)
                                        .addComponent(txtTransport)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblTransport)
                                    .addComponent(lblVehicleNo)
                                    .addComponent(lblStation)
                                    .addComponent(lblPo)
                                    .addComponent(lblNoofPacks)))
                            .addGroup(panelPrintLayout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel17)
                                    .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(panelPrintLayout.createSequentialGroup()
                                            .addComponent(jLabel21)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(lblGstin2))
                                        .addGroup(panelPrintLayout.createSequentialGroup()
                                            .addComponent(jLabel20)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(lblState2))
                                        .addComponent(lblShippedAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(panelPrintLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblGrandTotal)
                            .addComponent(lblDesc))
                        .addGap(196, 196, 196)
                        .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblRoundOff)
                            .addComponent(txtTotal))))
                .addGap(10, 10, 10))
            .addGroup(panelPrintLayout.createSequentialGroup()
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel25)
                    .addGroup(panelPrintLayout.createSequentialGroup()
                        .addComponent(jLabel22)
                        .addGap(18, 18, 18)
                        .addComponent(txtBankDetails, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(56, 56, 56)
                        .addComponent(lblBankDetails))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 402, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblRupee)
                    .addComponent(jLabel28))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(panelPrintLayout.createSequentialGroup()
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel26)
                    .addComponent(jLabel27))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel31))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPrintLayout.createSequentialGroup()
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(txtGstin)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPrintLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addGap(199, 199, 199))
        );
        panelPrintLayout.setVerticalGroup(
            panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPrintLayout.createSequentialGroup()
                .addComponent(jLabel4)
                .addGap(1, 1, 1)
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtGstin)
                    .addComponent(jLabel3))
                .addGap(14, 14, 14)
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtInvoiceNumber)
                    .addComponent(jLabel11)
                    .addComponent(txtTransport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTransport))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtDate)
                    .addComponent(jLabel12)
                    .addComponent(txtVehicleNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblVehicleNo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtPlaceOfSupply, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(txtStation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblStation)
                    .addComponent(lblPlace))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtReverse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(txtPoNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPo)
                    .addComponent(lblReverse))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtGrNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(txtNoofPacks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNoofPacks)
                    .addComponent(lblGrNo))
                .addGap(18, 18, 18)
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(jLabel17))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblShippedAddress, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblBilledAddress, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(lblState)
                    .addComponent(jLabel20)
                    .addComponent(lblState2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(lblGSTIN)
                    .addComponent(jLabel21)
                    .addComponent(lblGstin2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblActual, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDesc)
                    .addComponent(lblRoundOff))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTotal)
                    .addComponent(lblGrandTotal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblRupee)
                .addGap(26, 26, 26)
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(txtBankDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblBankDetails))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel23)
                    .addComponent(jLabel29))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(jLabel30))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel25)
                .addGroup(panelPrintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPrintLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel26)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel27))
                    .addGroup(panelPrintLayout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(jLabel31)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel28)
                .addGap(51, 51, 51))
        );

        jScrollPane3.setViewportView(panelPrint);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(278, 278, 278)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnPrint))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(btnPrint))
                .addGap(5, 5, 5)
                .addComponent(jScrollPane3))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        
        try{
             hideTextBox();
            Toolkit tkp = panelPrint.getToolkit();
            PrintJob pjp = tkp.getPrintJob(this, null, null);
            Graphics g = pjp.getGraphics();
            panelPrint.printAll(g);
            g.dispose();
            pjp.end();  
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnPrintActionPerformed

    private void txtVehicleNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtVehicleNoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVehicleNoActionPerformed

    private void txtTransportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTransportActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTransportActionPerformed

    private void txtGrNoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtGrNoKeyPressed
        lblGrNo.setText(txtGrNo.getText());
    }//GEN-LAST:event_txtGrNoKeyPressed

    private void txtGrNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtGrNoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtGrNoActionPerformed

    private void txtReverseKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtReverseKeyPressed
        lblReverse.setText(txtReverse.getText());
    }//GEN-LAST:event_txtReverseKeyPressed

    private void txtPlaceOfSupplyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPlaceOfSupplyKeyPressed
        lblPlace.setText(txtPlaceOfSupply.getText());
    }//GEN-LAST:event_txtPlaceOfSupplyKeyPressed

    private void txtPlaceOfSupplyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPlaceOfSupplyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPlaceOfSupplyActionPerformed

    private void tblBillComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_tblBillComponentResized
     
    }//GEN-LAST:event_tblBillComponentResized

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Print.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Print.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Print.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Print.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Print().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPrint;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblActual;
    private javax.swing.JLabel lblBankDetails;
    private javax.swing.JLabel lblBilledAddress;
    private javax.swing.JLabel lblDesc;
    private javax.swing.JLabel lblGSTIN;
    private javax.swing.JLabel lblGrNo;
    private javax.swing.JLabel lblGrandTotal;
    private javax.swing.JLabel lblGstin2;
    private javax.swing.JLabel lblNoofPacks;
    private javax.swing.JLabel lblPlace;
    private javax.swing.JLabel lblPo;
    private javax.swing.JLabel lblReverse;
    private javax.swing.JLabel lblRoundOff;
    private javax.swing.JLabel lblRupee;
    private javax.swing.JLabel lblShippedAddress;
    private javax.swing.JLabel lblState;
    private javax.swing.JLabel lblState2;
    private javax.swing.JLabel lblStation;
    private javax.swing.JLabel lblTransport;
    private javax.swing.JLabel lblVehicleNo;
    private javax.swing.JPanel panelPrint;
    private javax.swing.JTable tblBill;
    private javax.swing.JTable tblTax;
    private javax.swing.JTextField txtBankDetails;
    private javax.swing.JLabel txtDate;
    private javax.swing.JTextField txtGrNo;
    private javax.swing.JLabel txtGstin;
    private javax.swing.JLabel txtInvoiceNumber;
    private javax.swing.JTextField txtNoofPacks;
    private javax.swing.JTextField txtPlaceOfSupply;
    private javax.swing.JTextField txtPoNo;
    private javax.swing.JTextField txtReverse;
    private javax.swing.JTextField txtStation;
    private javax.swing.JLabel txtTotal;
    private javax.swing.JTextField txtTransport;
    private javax.swing.JTextField txtVehicleNo;
    // End of variables declaration//GEN-END:variables

    private final static int POINTS_PER_INCH = 72;
    
    private void hideLabels() {
        lblGrNo.setVisible(false);
        lblPlace.setVisible(false);
        lblReverse.setVisible(false);
        lblNoofPacks.setVisible(false);
        lblPo.setVisible(false);
        lblStation.setVisible(false);
        lblTransport.setVisible(false);
        lblVehicleNo.setVisible(false);
        lblState.setVisible(false);
        lblState2.setVisible(false);
        lblGSTIN.setVisible(false);
        lblGstin2.setVisible(false);
        txtStation.setText("Erode");
        lblBankDetails.setVisible(false);
    }
    private void hideTextBox(){
        lblReverse.setText(txtReverse.getText());
        lblGrNo.setText(txtGrNo.getText());
        lblPlace.setText(txtPlaceOfSupply.getText());
        lblNoofPacks.setText(txtNoofPacks.getText());
        lblPo.setText(txtPoNo.getText());
        lblStation.setText(txtStation.getText());
        lblTransport.setText(txtTransport.getText());
        lblVehicleNo.setText(txtVehicleNo.getText());
        txtGrNo.setVisible(false);
        txtPlaceOfSupply.setVisible(false);
        txtReverse.setVisible(false);
        txtNoofPacks.setVisible(false);
        txtPoNo.setVisible(false);
        txtStation.setVisible(false);
        txtTransport.setVisible(false);
        txtVehicleNo.setVisible(false);
        lblGrNo.setVisible(true);
        lblPlace.setVisible(true);
        lblReverse.setVisible(true);
        lblNoofPacks.setVisible(true);
        lblPo.setVisible(true);
        lblStation.setVisible(true);
        lblTransport.setVisible(true);
        lblVehicleNo.setVisible(true);
        txtBankDetails.setVisible(false);
        lblBankDetails.setText(txtBankDetails.getText());
        lblBankDetails.setVisible(true);
    }
    
}
