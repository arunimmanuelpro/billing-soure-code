/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Arun
 */
public class Database {
    
    private static Connection connection;
    public static Connection getConnections(){
        try{
            if(connection ==null || connection.isClosed()){

                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/billing_db", "root", "");
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    return connection;
}
    
}
